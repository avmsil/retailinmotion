﻿using RetailInMotion.Domain;
using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Contracts.Persistence
{
    public interface IOrderRepository: IAsyncRepository<Order>
    {
        Task<Order> GetOrderAsync(Guid orderId);
        Task UpdateOrderItemsAsync(Order order);
        Task<List<Order>> GetPagedOrdersAsync(int page, int size);
    }
}
