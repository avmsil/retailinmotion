﻿using MediatR;
using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetailInMotion.Application.Features.Orders.Commands.CreateOrder
{
    public class CreateOrderCommand: IRequest<Guid>
    {
        public Guid Id { get; set; }
        public Address DeliveryAddress { get; set; }
        public DateTime OrderPlaced { get; set; }
        public int Quantity { get; set; }
        public ICollection<Product> Items { get; set; }
    }
}
