﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandHandler: IRequestHandler<CreateOrderCommand, Guid>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateOrderCommand> _logger;

        public CreateOrderCommandHandler(IMapper mapper, IOrderRepository orderRepository, ILogger<CreateOrderCommand> logger)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
            _logger = logger;
        }

        public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
                var @order = _mapper.Map<Order>(request);
                @order = await _orderRepository.AddAsync(@order, cancellationToken);
                return order.Id;
            
        }
    }
}
