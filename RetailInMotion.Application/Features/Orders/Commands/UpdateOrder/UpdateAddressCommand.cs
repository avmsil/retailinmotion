﻿using MediatR;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Commands.UpdateOrder
{
    public class UpdateAddressCommand: IRequest
    {
        public Guid Id { get; set; }
        public Address DeliveryAddress { get; set; }
    }
}
