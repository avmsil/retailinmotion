﻿using AutoMapper;
using MediatR;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Application.Exceptions;
using RetailInMotion.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Commands.UpdateOrder
{
    public class UpdateAddressCommandHandler : IRequestHandler<UpdateAddressCommand>
    {
        private readonly IAsyncRepository<Order> _orderRepository;
        private readonly IMapper _mapper;

        public UpdateAddressCommandHandler(IMapper mapper, IAsyncRepository<Order> orderRepository)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<Unit> Handle(UpdateAddressCommand request, CancellationToken cancellationToken)
        {
            var orderToUpdate = await _orderRepository.GetByIdAsync(request.Id);

            if (orderToUpdate == null)
            {
                throw new NotFoundException(nameof(Order), request.Id);
            }

            _mapper.Map(request, orderToUpdate, typeof(UpdateAddressCommand), typeof(Order));

            await _orderRepository.UpdateAsync(orderToUpdate);

            return Unit.Value;
        }
    }
}
