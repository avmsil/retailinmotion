﻿using AutoMapper;
using MediatR;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Application.Exceptions;
using RetailInMotion.Domain;
using RetailInMotion.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Commands.UpdateOrder
{
    public class UpdateItemsCommandHandler: IRequestHandler<UpdateItemsCommand>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public UpdateItemsCommandHandler(IMapper mapper, IOrderRepository orderRepository)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<Unit> Handle(UpdateItemsCommand request, CancellationToken cancellationToken)
        {
            var orderToUpdate = await _orderRepository.GetByIdAsync(request.Id);

            if (orderToUpdate == null)
            {
                throw new NotFoundException(nameof(Order), request.Id);
            }

            _mapper.Map(request, orderToUpdate, typeof(UpdateItemsCommand), typeof(Order));

            await _orderRepository.UpdateOrderItemsAsync(orderToUpdate);

            return Unit.Value;
        }
    }
}
