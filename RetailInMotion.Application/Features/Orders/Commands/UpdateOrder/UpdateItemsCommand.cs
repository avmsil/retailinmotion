﻿using MediatR;
using RetailInMotion.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetailInMotion.Application.Features.Orders.Commands.UpdateOrder
{
    public class UpdateItemsCommand: IRequest
    {
        public Guid Id { get; set; }
        public ICollection<Product> Items { get; set; }
    }
}
