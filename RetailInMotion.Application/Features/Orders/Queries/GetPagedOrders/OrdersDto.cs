﻿using System;

namespace RetailInMotion.Application.Features.Orders.GetPagedOrders
{
    public class OrdersDto
    {
        public Guid Id { get; set; }
        public int OrderTotal { get; set; }
        public DateTime OrderPlaced { get; set; }
    }
}