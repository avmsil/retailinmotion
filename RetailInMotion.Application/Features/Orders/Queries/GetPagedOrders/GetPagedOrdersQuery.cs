﻿using MediatR;
using System;

namespace RetailInMotion.Application.Features.Orders.GetPagedOrders
{
    public class GetPagedOrdersQuery : IRequest<PagedOrdersVm>
    {
        public int Page { get; set; }
        public int Size { get; set; }
    }
}
