﻿using RetailInMotion.Application.Features.Orders.GetPagedOrders;
using System.Collections.Generic;

namespace RetailInMotion.Application.Features.Orders.GetPagedOrders
{
    public class PagedOrdersVm
    {
        public int Count { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public ICollection<OrdersDto> Orders{ get; set; }
    }
}