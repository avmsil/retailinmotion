﻿using AutoMapper;
using MediatR;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Application.Features.Orders.GetPagedOrders;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.GetOrdersForMonth
{
    public class GetPagedOrdersQueryHandler : IRequestHandler<GetPagedOrdersQuery, PagedOrdersVm>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetPagedOrdersQueryHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<PagedOrdersVm> Handle(GetPagedOrdersQuery request, CancellationToken cancellationToken)
        {
            var list = await _orderRepository.GetPagedOrdersAsync(request.Page, request.Size);
            var orders =  _mapper.Map<List<OrdersDto>>(list);

            return new PagedOrdersVm() { Count = orders.Count, Orders = orders, Page = request.Page, Size = request.Size };
        }
    }
}
