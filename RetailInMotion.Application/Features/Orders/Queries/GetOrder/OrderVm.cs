﻿using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Queries.GetOrder
{
    public class OrderVm
    {
        public Guid Id { get; set; }
        public Address DeliveryAddress { get; set; }
        public DateTime OrderPlaced { get; set; }
        public int Quantity { get; set; }
        public ICollection<Product> Items { get; set; }
    }
}
