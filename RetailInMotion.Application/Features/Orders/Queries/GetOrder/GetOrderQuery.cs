﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Queries.GetOrder
{
    public class GetOrderQuery: IRequest<OrderVm>
    {
        public Guid OrderId { get; set; }
    }
}
