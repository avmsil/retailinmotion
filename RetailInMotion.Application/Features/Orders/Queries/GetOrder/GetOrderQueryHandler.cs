﻿using AutoMapper;
using MediatR;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Application.Features.Orders.Queries.GetOrder
{
    public class GetOrderQueryHandler: IRequestHandler<GetOrderQuery, OrderVm>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetOrderQueryHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<OrderVm> Handle(GetOrderQuery request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.GetOrderAsync(request.OrderId);
            return _mapper.Map<OrderVm>(order);

        }
    }
}
