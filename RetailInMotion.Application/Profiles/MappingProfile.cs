﻿using AutoMapper;
using RetailInMotion.Application.Features.Orders.Commands;
using RetailInMotion.Application.Features.Orders.Commands.CreateOrder;
using RetailInMotion.Application.Features.Orders.Commands.UpdateOrder;
using RetailInMotion.Application.Features.Orders.GetPagedOrders;
using RetailInMotion.Application.Features.Orders.Queries.GetOrder;
using RetailInMotion.Domain;

namespace RetailInMotion.Application.Profiles
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Order, CreateOrderCommand>().ReverseMap();
            CreateMap<Order, UpdateAddressCommand>().ReverseMap();
            CreateMap<Order, UpdateItemsCommand>().ReverseMap();
            CreateMap<Order, OrdersDto>();
            CreateMap<Order, OrderVm>();
        }
    }
}
