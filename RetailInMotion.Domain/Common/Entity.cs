﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetailInMotion.Domain.Common
{
    public class Entity
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
