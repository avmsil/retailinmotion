﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetailInMotion.Domain.ValueObjects
{
    public class Address
    {
        public string EirCode { get; set; }
        public string StreetName { get; set; }
        public string County { get; set; }
        public string City { get; set; }
    }
}
