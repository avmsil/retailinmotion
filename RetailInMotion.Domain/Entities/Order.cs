﻿using RetailInMotion.Domain.Common;
using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace RetailInMotion.Domain
{
    public class Order: Entity
    {
        public Guid Id { get; set; }
        public Address DeliveryAddress { get; set; }
        public DateTime OrderPlaced { get; set; }
        public int Quantity { get; set; }
        public ICollection<Product> Items { get; set; }
    }
}
