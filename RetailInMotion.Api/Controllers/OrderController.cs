﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RetailInMotion.Application.Features.Orders.Commands;
using RetailInMotion.Application.Features.Orders.Commands.CreateOrder;
using RetailInMotion.Application.Features.Orders.Commands.UpdateOrder;
using RetailInMotion.Application.Features.Orders.GetPagedOrders;
using RetailInMotion.Application.Features.Orders.Queries.GetOrder;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("/getorder", Name = "GetOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<OrderVm>> GetOrder(Guid orderId)
        {
            var getOrderQuery= new GetOrderQuery { OrderId = orderId };
            var dto = await _mediator.Send(getOrderQuery);

            return Ok(dto);
        }

        [HttpGet("/getpagedorders", Name = "GetPagedOrders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<PagedOrdersVm>> GetPagedOrders(int page = 1, int size = 1)
        {
            var getOrdersForMonthQuery = new GetPagedOrdersQuery() { Page = page, Size = size };
            var dtos = await _mediator.Send(getOrdersForMonthQuery);

            return Ok(dtos);
        }

        [HttpPost("/cancelorder", Name = "CancelOrder")]
        public async Task<ActionResult<Guid>> Cancel([FromBody] CreateOrderCommand createOrderCommand, CancellationToken cancellationToken = default)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            cancellationToken = source.Token;
            source.CancelAfter(20);

            try
            {
                var id = await _mediator.Send(createOrderCommand, cancellationToken);
                return Ok(id);
            }
            catch (OperationCanceledException)
            {
                return BadRequest("operation was cancelled");
            }
        }


        [HttpPost("/addorder", Name = "AddOrder")]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateOrderCommand createOrderCommand)
        {
            var id = await _mediator.Send(createOrderCommand);
            return Ok(id);
        }

        [HttpPut("/updateaddress",Name = "UpdateOrderAddress")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> Update([FromBody] UpdateAddressCommand updateAddressCommand)
        {
            await _mediator.Send(updateAddressCommand);
            return NoContent();
        }

        [HttpPut("/updateitems", Name = "UpdateOrderItems")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> Update([FromBody] UpdateItemsCommand updateItemsCommand)
        {
            await _mediator.Send(updateItemsCommand);
            return NoContent();
        }
    }
}
