﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Persistence.Repositories;

namespace RetailInMotion.Persistence
{
    public static class PersistenceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RetailInMotionDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("RetailInMotionConnectionString")));

            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));

            services.AddScoped<IOrderRepository, OrderRepository>();
            //services.Decorate<IOrderRepository, CachedOrderRepository>();
            return services;    
        }
    }
}
