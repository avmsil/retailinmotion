﻿using Microsoft.EntityFrameworkCore;
using RetailInMotion.Application.Contracts.Persistence;
using RetailInMotion.Domain;
using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RetailInMotion.Persistence.Repositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(RetailInMotionDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Order> GetOrderAsync(Guid orderId)
        {
            return await _dbContext.Orders
                .Include(o => o.Items)
                .FirstOrDefaultAsync(o => o.Id == orderId);
        }

        public async Task UpdateOrderItemsAsync(Order order)
        {
            var orderToUpdate = await _dbContext.Orders
                                .Include(o => o.Items)
                                .FirstOrDefaultAsync(o => o.Id == order.Id);
            _dbContext.Entry(orderToUpdate).CurrentValues.SetValues(order);

            foreach (var item in order.Items)
            {
                var product = order.Items
                        .AsQueryable()
                        .FirstOrDefault(p => p.Id == item.Id);
                
                if (product != null)
                {
                    _dbContext.Entry(product).CurrentValues.SetValues(item);
                }
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Order>> GetPagedOrdersAsync(int page, int size)
        {
            return await _dbContext.Orders
                .Skip((page - 1) * size).Take(size).AsNoTracking().ToListAsync();
        }

    }
}
