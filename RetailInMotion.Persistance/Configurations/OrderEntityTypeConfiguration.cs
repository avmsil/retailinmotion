﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RetailInMotion.Domain;
using RetailInMotion.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetailInMotion.Persistance.Configurations
{
    public class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> orderConfiguration)
        {
            orderConfiguration.HasKey(o => o.Id);
            orderConfiguration.OwnsOne(o => o.DeliveryAddress).ToTable("Address");
            orderConfiguration.OwnsOne(
                o => o.DeliveryAddress, a =>
                {
                    a.WithOwner().HasForeignKey("OrderId");
                    a.Property<int>("Id").IsRequired().UseIdentityColumn();
                    a.HasKey("Id");
                });
        }
    }
}
