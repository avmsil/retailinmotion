﻿using Microsoft.EntityFrameworkCore;
using RetailInMotion.Domain;
using RetailInMotion.Domain.Common;
using RetailInMotion.Domain.Entities;
using RetailInMotion.Domain.ValueObjects;
using RetailInMotion.Persistance.Configurations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RetailInMotion.Persistence
{
    public class RetailInMotionDbContext : DbContext
    {
        public RetailInMotionDbContext(DbContextOptions<RetailInMotionDbContext> options)
           : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        //public DbSet<Address> Adresses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());

            var order1 = Guid.Parse("{F5A6A3A0-4227-4973-ABB5-A63FBE725923}");
            var order2 = Guid.Parse("{BA0EB0EF-B69B-46FD-B8E2-41B4178AE7CB}");


            var product1 = Guid.Parse("{B0788D2F-8003-43C1-92A4-EDC76A7C5DDE}");
            var product2 = Guid.Parse("{6313179F-7837-473A-A4D5-A5571B43E6A6}");
            var product3 = Guid.Parse("{BF3F3002-7E53-441E-8B76-F6280BE284AA}");

            modelBuilder.Entity<Order>(o =>
            {
                o.HasData(new
                {
                    Id = order1,
                    Quantity = 1,
                    OrderPlaced = DateTime.Now,
                    CreatedDate = DateTime.Now
                });


                o.OwnsOne(e => e.DeliveryAddress).HasData(new
                {
                    Id = 1,
                    City = "Dublin",
                    County = "Dublin",
                    EirCode = "XP1 223",
                    StreetName = "223 Glasaree Rd",
                    OrderId = order1
                });

                o.HasData(new
                {
                    Id = order2,
                    OrderPlaced = DateTime.Now,
                    Quantity = 2,
                    CreatedDate = DateTime.Now
                });


                o.OwnsOne(e => e.DeliveryAddress).HasData(new
                {
                    Id = 2,
                    City = "Cork",
                    County = "Cork",
                    EirCode = "XP2 555",
                    StreetName = "555 Grove Rd",
                    OrderId = order2
                });
            });

            modelBuilder.Entity<Product>().HasData(new Product
            {
                Id = product1,
                OrderId = order1
            });
            modelBuilder.Entity<Product>().HasData(new Product
            {
                Id = product2,
                OrderId = order2
            });
            modelBuilder.Entity<Product>().HasData(new Product
            {
                Id = product3,
                OrderId = order1
            });

        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<Entity>())
            {
                entry.Entity.CreatedDate = DateTime.Now;
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedDate = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedDate = DateTime.Now;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
